#!/usr/bin/python3

import copy, json, os, requests
from pprint import pprint

villages_request = requests.get(
		'https://events.ccc.de/camp/2015/wiki/index.php?title=Special:Ask',
		params=(
			('q', '[[Category:Village]]'),
			('po', "\r\n".join([
				'?Has description',
				'?Has website',
				'?Has contact',
				'?Subvillage of',
				'?Related to village',
				'?Has village tag',
				'?Provides session location',
				'?Has orga contact',
				'?Has citizen count',
				'?Needs network access',
				'?Villages describe network needs',
				'?Needs power',
				'?Has power needs comment',
				'?Village plans',
				'?Village comments plans',
				'?Has village constraints',
				'?Likes to rent a tent',
				'?Describing tents to rent',
				'?Likes to rent chairs',
				'?Likes to rent tables',
				'?Provides transport for',
				'?Has plans with tracks',
				'?Size needed min',
				'?Size needed max',
				'?Has order interest',
				'?Has planning notes'])
			),
			('p[format]', 'json'),
			('p[limit]', 500),
		),
		verify=False #'cacert.pem'
	)


originData = villages_request.json()['results']

data = {}

#pprint(originData)


def parse_printouts(village, originPrintouts):
	for k,v in originPrintouts.items():
		key = map_printouts(k)

		if key.endswith('_size'):
			if len(v) != 0: 
				village[key] = v[0][0]
			continue

# TODO special handlings

		if key == 'place_subvillage_of':
			if len(v) != 0:
				village[key] = v[0]['fulltext'].replace('Village:', '')
			continue

		if len(v) == 1:
			village[key] = v[0]
		elif  len(v) > 1:
			village[key] = v


def map_printouts(x):
	return {
		'Has citizen count': 'size_member',
		'Size needed min': 'size_min',
		'Size needed max': 'size_max',

		'Has orga contact': 'contact_orga',
		'Has contact': 'contact_public',
		'Has website': 'contact_website',
		'Has description': 'text_description',
		'Village comments plans': 'text_plans_comments',
		'Has planning notes': 'text_planning_notes',

		'Related to village': 'place_related_to',
		'Subvillage of': 'place_subvillage_of',
		'Has plans with tracks': 'place_near_tracks',
		'Provides session location': 'place_session_location',

		'Likes to rent a tent': 'rent_tent',
		'Describing tents to rent': 'rent_tent_notes',
		'Likes to rent tables': 'rent_tables',
		'Likes to rent chairs': 'rent_chairs',

		'Needs network access': 'net_size',
		'Villages describe network needs': 'net_notes',

		'Needs power': 'power_size',
		'Has power needs comment': 'power_notes',

# TODO better keys
		'Has village tag': '_tags',
		'Has order interest': '_central_order_interest',
		'Provides transport for': '_transport',
	}[x]



def parse_plans(village, originPlans):
	for i in originPlans:
		key = map_plans(i)
		village[key] = True

def map_plans(x):
	return {
		'... build up a big tent': 'size_big_tent',
		'... play loud music and party': 'noise_loud_music',

		'... bring light installations': 'installations_light',
		'... bring some special machines': 'installations_machines',
		'... have any other unusual installations': 'installations_other',

		'... run a kitchen for our citizens': 'plans_kitchen',
		'... run a (public) kitchen': 'old_plans_kitchen',
		'... run a bar for our citizens': 'plans_bar',
		'... run a (public) bar': 'old_plans_bar',

		'... install a fridge or a freezer': 'plans_fridge',
		'... run any other Air Condition device': 'plans_air_condition',
	}[x]



def parse_constraints(village, originConstraints):
	for i in originConstraints:
		key = map_constraints(i)
		if key.startswith('no_'):
			village[key.replace('no_', '', 1)] = False
		else:
			village[key] = True

def map_constraints(x):
	return {
		'We are a quiet village and will not make much noise. So we like to be placed on a quiet location.': 'noise_quiet',
		'We do not make party ourselves but like to be placed near the party villages.': 'noise_party_near',
		'We do not make party but will make some noise.': 'noise_some',
		'We are playing music and like to party': 'noise_party_make',

		'We will provide space to hang out for other camp participants on our village': 'public_space',
		'We prefer to be on our own, not providing any special place for visitors': 'no_public_space',

		'We like to be placed on a central location where many people are crossing': 'place_central',
		'We like to be placed on a non-central spot where less people are coming by': 'no_place_central',
		'We do not like to be visited by representitives of the press, so a non-central location would be better': 'place_press',
		'We do not like to be filmed or photographed': 'no_place_press',
		'We like to show the stuff we do': 'place_show_stuff',
		'We are just a group of people hanging out, we wont show any special projects': 'no_place_show_stuff',

		'We would like to have trees on our village area': 'place_trees',
		'We would prefer a spot near the water': 'place_water',
	}[x]



def existValueAndRemove(array, value):
	retVal = value in array
	if retVal:
		array.remove(value)
	return retVal



for originName,originVillage in originData.items():
	name = originName.replace("Village:", "")
	village = {"name":name}

	if name == "Example Village": continue

	originVillage = dict(originVillage)
	village["contact_wiki"] = "https:" + originVillage.pop("fullurl")
	
	del originVillage["fulltext"]
	del originVillage["exists"]
	del originVillage["namespace"]

	originPrintouts = originVillage.pop("printouts")

	if len(originVillage) != 0:
		print("originVillage " + name + " isn't empty:")
		pprint(originVillage)
		quit()


	parse_plans(village, originPrintouts.pop('Village plans'))
	parse_constraints(village, originPrintouts.pop('Has village constraints'))

	parse_printouts(village, originPrintouts)


	for k,v in copy.deepcopy(village).items():
		if v == "No":
			village[k] = False
		elif v == "Yes":
			village[k] = True
#		if k.startswith('rent_') and ( v == 0 or v == False):
#			del village[k]

	data[name] = village



os.makedirs('data', exist_ok=True)
with open('data/villages.json', 'w') as outfile:
	json.dump(data, outfile, sort_keys=True, ensure_ascii=False, indent=4)


#EOF