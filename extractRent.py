#!/usr/bin/python3

import json, sys

villages = json.load(open("data/villages.json"))

out = {}

sumChairs = 0
sumTables = 0

for name,v in villages.items():
	if ('rent_tent' in v and v['rent_tent'] is True
			or 'rent_tent_notes' in v and v['rent_tent_notes'] != ''
			or 'rent_tables' in v and v['rent_tables'] > 0
			or 'rent_chairs' in v and v['rent_chairs'] > 0):

		outv = {}

		if 'rent_tent' in v and v['rent_tent']:
			outv['tent'] = True

		if 'rent_tent_notes' in v:
			outv['notes'] = v['rent_tent_notes']

		if 'rent_tables' in v and v['rent_tables'] > 0:
			outv['tables'] = v['rent_tables']
			sumTables += int(v['rent_tables'])

		if 'rent_chairs' in v and v['rent_chairs'] > 0:
			outv['chairs'] = v['rent_chairs']
			sumChairs += int(v['rent_tables'])

		out[v['name']] = outv

json.dump(out, sys.stdout, sort_keys=True, ensure_ascii=False, indent=4)

print("sumTables="+ str(sumTables) + " sumChairs=" + str(sumChairs))


#EOF
