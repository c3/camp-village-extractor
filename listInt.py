#!/usr/bin/python3

import json, sys

villages = json.load(open("data/villages.json"))

out = {}

ints = [
	'Anarchist',
	'Bitbureauet',
	'CryptoParty',
	'Digitalcourage',
	'Dyne',
	'Free Software Foundation Europe',
	'Freifunk',
	'Geraffel',
	'Italian Embassy',
	'La Quadrature du Camp',
	'Noisy Square',
	'Norton\'s Obscure Phoggy Embassy',
	'Queer Feminist Geeks',
	'Selfnet',
	'Spanish village',
	'Tactical centre for courage',
	'ÜberWall',
	'Youbroketheinternet']

sumSize = 0
sumMember = 0

for name,v in villages.items():
	if name in ints:

		outv = {}
		#outv['wiki'] = v['contact_wiki']

		if 'size_member' in v:
			outv['size_member'] = v['size_member']
			sumMember += int(v['size_member'])

		if 'size_min' in v:
			outv['size_min'] = v['size_min']
			sumSize += int(v['size_min'])

		out[v['name']] = outv


json.dump(out, sys.stdout, sort_keys=True, ensure_ascii=False, indent=4)

print("Summe Villages:	", len(ints))
print("Summe Size:	", sumSize)
print("Summe Member:	", sumMember)

#EOF
