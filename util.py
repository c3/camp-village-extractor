

def mapifexist(key, source, target):
	if key in source:
		target[key] = source[key]


def calcloudness(village):
	loudness = 0

	if 'noise_loud_music' in village:
		if village['noise_loud_music'] is True:
			loudness += 2
		else:
			loudness -= 0
	if 'noise_party_make' in village:
		if village['noise_party_make'] is True:
			loudness += 2
		else:
			loudness -= 0
	if 'noise_party_near' in village:
		if village['noise_party_near'] is True:
			loudness += 1
		else:
			loudness -= 1
	if 'noise_quiet' in village:
		if village['noise_quiet'] is True:
			loudness += -2
		else:
			loudness -= -2
	if 'noise_some' in village:
		if village['noise_some'] is True:
			loudness += 1
		else:
			loudness -= 1

	return loudness



def calcpublicy(village):
	publicy = 0

	if 'place_central' in village:
		if village['place_central'] is True:
			publicy += 2
		else:
			publicy -= 1
	if 'place_press' in village:
		if village['place_press'] is True:
			publicy += 3
		else:
			publicy -= 2
	if 'place_show_stuff' in village:
		if village['place_show_stuff'] is True:
			publicy += 2
		else:
			publicy -= 1

	return publicy
