#!/usr/bin/python3

import json, sys

villages = json.load(open("data/villages.json"))


keyList = {}


for name,vi in villages.items():
	for key,va in vi.items():
		if key in keyList:
			keyList[key] = keyList[key] + 1
		else:
			keyList[key] = 1


json.dump(keyList, sys.stdout, sort_keys=True, ensure_ascii=False, indent=4)

#EOF