#!/usr/bin/python3

import json, sys
from util import *

villages = json.load(open("data/villages.json"))

out = {}

for name,v in villages.items():
		outv = calcloudness(v)

		if outv > 3:
			out[v['name']] = outv

json.dump(out, sys.stdout, sort_keys=True, ensure_ascii=False, indent=4)


#EOF
