#!/usr/bin/python3

import json, sys

villages = json.load(open("data/villages.json"))

out = {}

for name,v in villages.items():
	if ('place_related_to' in v
			or 'place_subvillage_of' in v):


		outv = {}
		#outv['wiki'] = v['contact_wiki']

		if 'place_subvillage_of' in v:
			outv['place_subvillage_of'] = v['place_subvillage_of']

		if 'place_related_to' in v:
			outv['place_related_to'] = v['place_related_to']

		out[v['name']] = outv


json.dump(out, sys.stdout, sort_keys=True, ensure_ascii=False, indent=4)

#EOF
