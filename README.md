Export village data from camp 2015 wiki and clean up the export

First update local "database" `data/villages.json` with:

`./prepare.py`

There are some example scripts for extract data, e.g. `extractSimple.py`
