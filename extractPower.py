#!/usr/bin/python3

import json, sys
from util import *

villages = json.load(open("data/villages.json"))

out = {}

for name,v in villages.items():
		outv = {}
		mapifexist('contact_wiki', v, outv)
		mapifexist('power_size', v, outv)
		mapifexist('power_notes', v, outv)
		mapifexist('size_member', v, outv)

		out[v['name']] = outv

json.dump(out, sys.stdout, sort_keys=True, ensure_ascii=False, indent=4)

#EOF
