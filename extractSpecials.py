#!/usr/bin/python3

import json, sys
from util import *

villages = json.load(open("data/villages.json"))

out = {}

for name,v in villages.items():
	if (
			'size_member' in v and v['size_member'] > 50
			or 'power_size' in v and int(v["power_size"]) > 3
			or 'size_min' in v and int(v["size_min"]) > 500):

			outv = {}
			outv['wiki'] = v['contact_wiki']

			mapifexist('power_size', v, outv)
			mapifexist('power_notes', v, outv)
			mapifexist('size_member', v, outv)
			mapifexist('size_min', v, outv)

			outv['loudness'] = calcloudness(v)
			outv['publicy'] = calcpublicy(v)

			out[v['name']] = outv


#for name,v in out.items():
#	print(name + " " + str(v['size_member']) + " L" + str(v['loudness'])
#			+ " P" + str(v['publicy']))

json.dump(out, sys.stdout, sort_keys=True, ensure_ascii=False, indent=4)

#EOF
